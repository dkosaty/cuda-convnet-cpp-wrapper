#ifndef LAYERPARAMS_H
#define LAYERPARAMS_H

#include <string>

#include <Python.h>

class LayerParams
{
public:
    LayerParams();

    PyObject *getDictionary();

    void set(std::string key, std::string value);

private:
    PyObject *dictionary_;
};

#endif // LAYERPARAMS_H
