#ifndef LISTLAYERPARAMS_H
#define LISTLAYERPARAMS_H

#include <ostream>

#include <boost/shared_ptr.hpp>

#include "LayerParams.h"

using namespace boost;

class ListLayerParams
{
public:
    ListLayerParams();

    boost::shared_ptr<PyListObject> getLayerParams();

    void append(shared_ptr<LayerParams> item);

    friend std::ostream &operator<<(std::ostream &output, shared_ptr<ListLayerParams> list);

private:
    PyListObject *dictionaries_;
};

#endif // LISTLAYERPARAMS_H
