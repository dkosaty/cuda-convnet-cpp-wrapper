#include <iostream>
#include <iomanip>

#include <boost/make_shared.hpp>

#include "ListLayerParams.h"

using namespace std;

ListLayerParams::ListLayerParams()
{
    this->dictionaries_ = (PyListObject*)PyList_New(0);
}

boost::shared_ptr<PyListObject> ListLayerParams::getLayerParams()
{
    return boost::make_shared<PyListObject>(*this->dictionaries_);
}

void ListLayerParams::append(boost::shared_ptr<LayerParams> item)
{
    int ret = PyList_Append((PyObject*)this->dictionaries_, item->getDictionary());

    if (ret == -1)
        throw ret;
}

ostream &operator<<(ostream &output, boost::shared_ptr<ListLayerParams> list)
{
    for (int i = 0; i < Py_SIZE(list->dictionaries_); ++i)
    {
//        char *item = PyString_AsString(PyObject_Str(PyList_GET_ITEM(list->dictionaries_, i)));

//        cout << item << endl;

        PyObject *dict = PyList_GET_ITEM(list->dictionaries_, i);

        PyObject *keys = PyDict_Keys(dict);

        string title = PyString_AsString(PyObject_Str(PyDict_GetItemString(dict, "name")));

        cout << title << endl;

        for (int j = 0; j < Py_SIZE(keys); ++j)
        {
            string key = PyString_AsString(PyObject_Str(PyList_GET_ITEM(keys, j)));

            string value = PyString_AsString(PyObject_Str(PyDict_GetItemString(dict, key.c_str())));

            if (key != "name")
                cout << ' ' << left << setw(15) << key << value << endl;
        }
    }

    return output;
}
