#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/foreach.hpp>

//#include <opencv2/highgui/highgui.hpp>

#include "ListLayerParams.h"

using namespace std;
//using namespace cv;

//void trainNet(PyListObject *params, Mat &image);

boost::shared_ptr<PyListObject> parseLayerParams()
{
    using boost::property_tree::ptree;

    boost::shared_ptr<ListLayerParams> list = boost::make_shared<ListLayerParams>();

    ptree pt;

    boost::property_tree::ini_parser::read_ini("layers.cfg", pt);

    BOOST_FOREACH (ptree::value_type &section, pt)
    {
        boost::shared_ptr<LayerParams> dict = boost::make_shared<LayerParams>();

        string title = section.first;

        dict->set("name", title);

        BOOST_FOREACH (ptree::value_type &content, pt.get_child(title))
        {
            string key = content.first;

            string value = content.second.data();

            dict->set(key, value);
        }

        list->append(dict);
    }

    cout << list << endl;

    return list->getLayerParams();
}

int main(int argc, char *argv[])
{
    int ret = EXIT_SUCCESS;

    try {
        boost::shared_ptr<PyListObject> layer_params = parseLayerParams();

//            Mat image = imread("/home/dmitry/datasets/INRIAPerson/Train/pos/crop001001.png");

//            imshow("", image);
//            waitKey();
//            destroyWindow("");

//            trainNet(layer_params, image);
    }
    catch (boost::property_tree::ptree_error &error) {
        cerr << "Exception: " << error.what() << endl;
        ret = EXIT_FAILURE;
        throw;
    }
    catch (int ret) {
        cerr << "Exception: " << ret << endl;
        ret = EXIT_FAILURE;
        throw;
    }

    return ret;
}
