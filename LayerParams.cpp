#include "LayerParams.h"

using namespace std;

LayerParams::LayerParams()
{
    this->dictionary_ = PyDict_New();
}

PyObject *LayerParams::getDictionary()
{
    return this->dictionary_;
}

void LayerParams::set(std::string key, std::string value)
{
    PyObject *py_value = Py_BuildValue("s", value.c_str());

    int ret = PyDict_SetItemString(this->dictionary_, key.c_str(), py_value);

    if (ret == -1)
        throw ret;
}
